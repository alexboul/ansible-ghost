# Installation de Ghost avec Ansible

*Alexandre BOULENOUAR*

## Introduction

*Il est important que je préface ces instructions en disant que je n'ai pas réussi à complètement déployer l'application Ghost.
J'ai rencontré des difficultés lors de l'installation, notamment avec les différentes options de configuration que je n'ai pas réussi à toutes mettre en place correctement.
J'ai décidé de tout de même présenter les étapes qui mènent jusqu'à l'installation de Ghost dans ce playbook Ansible.*

Ce projet a pour but de déployer une application Ghost sur un serveur Ubuntu avec Ansible. 
Vous trouverez dans ce fichier les instructions pour lancer le playbook Ansible qui permettra d'installer Ghost ainsi que tous les prérequis nécessaires à son bon fonctionnement.
Ce fichier contient également les explications des étapes du playbook et des différents fichiers composant ce projet.

## Prérequis

- Ubuntu (16.04, 18.04, 20.04 ou 22.04)
- Être connecté avec un utilisateur (qui n'est pas root) avec des privilèges sudo

## Lancement de l'installation

En étant à la racine du projet (envoyé par mail ou disponible sur [mon repo GitLab](https://gitlab.com/alexboul/ansible-ghost)), entrer la commande suivante :

```bash
ansible-playbook install_ghost.yml
```

Cette commande va lancer le playbook Ansible et va procéder à plusieurs étapes qui seront décrites plus bas.

## Fichiers du projet

### ansible.cfg

Le fichier `ansible.cfg` contient l'inventaire utilisé par le playbook. Cela permet de ne pas avoir à préciser l'inventaire dans la commande avec le paramètre `-i`. A modifier dans le fichier si vous souhaitez utiliser un autre inventaire.
Ce fichier contient également un paramètre `stdout_callback` qui permet d'afficher les résultats (et surtout les erreurs...) sur la console de manière plus lisible.

### inventory.ini

Le fichier `inventory.ini` contient la liste des serveurs sur lesquels le playbook va être exécuté (localhost dans notre cas). Il contient également les variables utilisées par le playbook, notamment la variable `ansible_connection`, qui permet de se passer de précisier dans la commande avec le paramètre `-c`

### install_ghost.yml

Le fichier `install_ghost.yml` est le playbook Ansible qui va être exécuté. Il est très court et ne contient que quelques instructions clés, notamment le role `ghost`, qui va définir toutes les étapes qui vont être exécutées.
L'installation des prérequis et le déploiement de l'application auraient pu être découpés en plusieurs rôles différents, mais j'ai préféré les regrouper dans un seul rôle pour plus de simplicité étant donné que les rôles ne contiennent que peu d'étapes, et que la spécificité des étapes donne peu de chance d'être réutilisables ailleurs.

### roles/ghost/tasks

Le dossier `roles/ghost/tasks` contient le fichier qui va dicter toutes les étapes du rôle ghost. 
*Si l'on avait voulu créer un autre rôle avec d'autres étapes, il aurait fallu créer un dossier `roles/autre_role/tasks` et y placer un fichier `main.yml` qui contiendrait les étapes du rôle.*

Notre fichier `main.yml` contient donc les étapes suivantes :

#### Apt update and upgrade

Cette étape permet de mettre à jour la liste des packages disponibles et de mettre à jour les packages installés.
L'option `cache_valid_time: 86400` permet de sauter cette étape si la mise à jour a déjà été effectuée dans les dernières 24h.

#### Installing NGINX

Cette étape permet d'installer le serveur web NGINX.


#### Opening Firewall

Cette étape permet d'ouvrir le firewall pour accepter les connections `HTTP` et `HTTPS`.

#### Installing MySQL

Cette étape va installer le package `mysql-server` qui va permettre d'utiliser une base de données MySQL pour Ghost.

#### Getting the GPG key for NodeJS LTS, Getting the nodesource repo & Installing NodeJS

Ces trois étapes mènent à l'installation d'une version supportée de NodeJS.

Avant le 27 octobre 2023, Ghost supportait les versions 16.x et 18.x de NodeJS. Depuis cette date, il ne supporte plus que les versions 18.x. 
Ces étapes permettent de récupérer et installer une version de NodeJS supportée par Ghost, ce qui n'est pas possible via le module `apt`, qui installe une version 12.x de NodeJS.

#### Installing npm

Cette étape permet d'installer npm (Node Package Manager) qui va permettre d'installer la CLI de Ghost

#### Installing Ghost CLI

Cette étape va installer la CLI de Ghost, qui va permettre d'installer Ghost et de le configurer.

#### Creating directory for Ghost (if doesnt exist)

Cette étape va créer un dossier dans le répertoire `/var/www` pour exposer l'application grâce à NGINX.

#### Changing directory ownership

Cette étape va changer le propriétaire du dossier `/var/www/ghostapp` pour que Ghost puisse installer tous les fichiers nécessaires.

#### Installing Ghost

Cette étape va, en une ligne de commande, installer Ghost dans le dossier `/var/www/ghostapp` et le configurer avec les options passées en paramètre de la commande.

C'est à cette étape que j'ai rencontré des difficultés et que vous devriez voir apparaître une erreur.